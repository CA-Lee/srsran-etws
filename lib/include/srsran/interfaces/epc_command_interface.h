/**
 * Copyright 2013-2022 Software Radio Systems Limited
 *
 * This file is part of srsRAN.
 *
 * srsRAN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * srsRAN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License can be found in
 * the LICENSE file in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/.
 *
 */

#ifndef SRSRAN_EPC_COMMAND_INTERFACE_H
#define SRSRAN_EPC_COMMAND_INTERFACE_H

namespace srsepc {

class mme_command_interface
{
public:
  virtual void send_etws_warning() = 0;
  virtual void send_cmas_warning() = 0;
};

} // namespace srsepc

#endif // SRSRAN_EPC_COMMAND_INTERFACE_H