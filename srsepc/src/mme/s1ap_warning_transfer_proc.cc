/**
 * Copyright 2013-2022 Software Radio Systems Limited
 *
 * This file is part of srsRAN.
 *
 * srsRAN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * srsRAN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License can be found in
 * the LICENSE file in the top-level directory of this distribution
 * and at http://www.gnu.org/licenses/.
 *
 */

#include "srsepc/hdr/mme/s1ap_warning_transfer_proc.h"
#include "srsepc/hdr/mme/s1ap.h"

namespace srsepc {

s1ap_warning_transfer_proc* s1ap_warning_transfer_proc::m_instance    = NULL;
pthread_mutex_t             s1ap_warning_transfer_proc_instance_mutex = PTHREAD_MUTEX_INITIALIZER;

s1ap_warning_transfer_proc::s1ap_warning_transfer_proc()
{
  return;
}

s1ap_warning_transfer_proc::~s1ap_warning_transfer_proc()
{
  return;
}

s1ap_warning_transfer_proc* s1ap_warning_transfer_proc::get_instance()
{
  pthread_mutex_lock(&s1ap_warning_transfer_proc_instance_mutex);
  if (NULL == m_instance) {
    m_instance = new s1ap_warning_transfer_proc();
  }
  pthread_mutex_unlock(&s1ap_warning_transfer_proc_instance_mutex);
  return (m_instance);
}

void s1ap_warning_transfer_proc::cleanup()
{
  pthread_mutex_lock(&s1ap_warning_transfer_proc_instance_mutex);
  if (NULL != m_instance) {
    delete m_instance;
    m_instance = NULL;
  }
  pthread_mutex_unlock(&s1ap_warning_transfer_proc_instance_mutex);
}

void s1ap_warning_transfer_proc::init()
{
  m_s1ap = s1ap::get_instance();
}

bool s1ap_warning_transfer_proc::send_write_replace_warning_request(struct sctp_sndrcvinfo enb_sri)
{
  s1ap_pdu_t tx_pdu;
  tx_pdu.set_init_msg().load_info_obj(ASN1_S1AP_ID_WRITE_REPLACE_WARNING);
  asn1::s1ap::write_replace_warning_request_s& write_replace_warning_request =
      tx_pdu.init_msg().value.write_replace_warning_request();

  write_replace_warning_request->msg_id.value.from_number(0x1100);     // 0x1100
  write_replace_warning_request->serial_num.value.from_number(0x3000); // 7..1 0011000000000000
  // write_replace_warning_request->warning_area_list
  write_replace_warning_request->repeat_period.value           = 10;
  // write_replace_warning_request->extended_repeat_period
  write_replace_warning_request->numof_broadcast_request.value = 10;
  // msg_id;
  // serial_num;
  // warning_area_list;
  // repeat_period;
  // extended_repeat_period;
  // numof_broadcast_request;
  // warning_type;
  // warning_security_info;
  // data_coding_scheme;
  // warning_msg_contents;
  // concurrent_warning_msg_ind;
  // warning_area_coordinates;

  if (!m_s1ap->s1ap_tx_pdu(tx_pdu, &enb_sri)) {
    m_logger.error("Error sending Write-Replace Warning Request.");
    return false;
  }
  m_logger.info("Sent Write-Replace Warning Request.");
  return true;
}

} // namespace srsepc